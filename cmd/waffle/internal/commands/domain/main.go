package domain

import (
	"context"
	"embed"
	"fmt"
	"path/filepath"
	"time"

	"gitlab.com/gorib/waffle/cmd/waffle/internal/pkg/command"
	"gitlab.com/gorib/waffle/cmd/waffle/internal/pkg/plural"
)

//go:embed templates/*
var templates embed.FS

func New() *cmd {
	return &cmd{command.New("make:domain", "Make domain with entity, repository and management", templates, nil)}
}

type cmd struct {
	*command.Command
}

func (c *cmd) Run(ctx context.Context, args ...string) error {
	if len(args) == 0 {
		return fmt.Errorf("usage: waffle %s <domain>", c.Code())
	}
	domain := args[0]
	projectName, err := c.FindProjectName()
	if err != nil {
		return err
	}

	params := map[string]string{
		"Domain":  domain,
		"Project": projectName,
	}

	path := filepath.Join("internal", "domain", domain)
	sources := map[string]string{
		filepath.Join("templates", "model", "entity.go.tpl"):      filepath.Join(path, "model", domain+".go"),
		filepath.Join("templates", "model", "errors.go.tpl"):      filepath.Join(path, "model", "errors.go"),
		filepath.Join("templates", "repository", "entity.go.tpl"): filepath.Join(path, "repository", domain+".go"),
		filepath.Join("templates", "repository", "stored.go.tpl"): filepath.Join(path, "repository", "stored_"+domain+".go"),
		filepath.Join("templates", "service", "entity.go.tpl"):    filepath.Join(path, "service", domain+".go"),
		filepath.Join("templates", "migration.sql.tpl"):           filepath.Join("tools", "migrate", "migrations", fmt.Sprintf("%s_create_%s_table.sql", time.Now().Format("20060102150405"), plural.Plural(domain))),
		filepath.Join("templates", "di.go.tpl"):                   filepath.Join("config", "di_domain_"+domain+".go"),
	}

	return c.Write(sources, params)
}
