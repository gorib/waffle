module gitlab.com/gorib/waffle

go 1.23.0

require (
	github.com/fatih/color v1.18.0
	github.com/gofiber/fiber/v2 v2.52.6
	github.com/pborman/getopt/v2 v2.1.0
	github.com/pressly/goose/v3 v3.24.1
	gitlab.com/gorib/di v0.0.0-20250224143814-d96e28d77cfd
	gitlab.com/gorib/pry v0.0.0-20250224143323-d269032b7208
	gitlab.com/gorib/server v0.0.0-20250224144501-1a385c4c1e37
	gitlab.com/gorib/session v0.0.0-20250224141908-df66ea5a990f
	gitlab.com/gorib/storage v0.0.0-20250224143814-9b7bf5108565
	gitlab.com/gorib/try v0.0.0-20250224141908-512acf4daa35
	golang.org/x/exp v0.0.0-20250218142911-aa4b98e5adaa
	google.golang.org/grpc v1.70.0
)

require (
	github.com/andybalholm/brotli v1.1.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/jmoiron/sqlx v1.4.0 // indirect
	github.com/klauspost/compress v1.18.0 // indirect
	github.com/mattn/go-colorable v0.1.14 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/mfridman/interpolate v0.0.2 // indirect
	github.com/rabbitmq/amqp091-go v1.10.0 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/rs/zerolog v1.33.0 // indirect
	github.com/sethvargo/go-retry v0.3.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.59.0 // indirect
	gitlab.com/gorib/criteria v0.0.0-20250224141747-7755bfaa022c // indirect
	gitlab.com/gorib/parser v0.0.0-20250224144300-e9d1752a4158 // indirect
	gitlab.com/gorib/ptr v0.0.0-20250202074625-505b05714bce // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/net v0.35.0 // indirect
	golang.org/x/sync v0.11.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/text v0.22.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20250219182151-9fdb1cabc7b2 // indirect
	google.golang.org/protobuf v1.36.5 // indirect
)
